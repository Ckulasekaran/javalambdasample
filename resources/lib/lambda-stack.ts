import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';


export class LambdaStack extends cdk.Stack {
    public readonly lambdaCode: lambda.CfnParametersCode;
    constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    this.lambdaCode = lambda.Code.fromCfnParameters();

    // The code that defines your stack goes here
    const func = new lambda.Function(this, 'Cortex-Activator-Lambda', {
      code: this.lambdaCode,
      runtime: lambda.Runtime.JAVA_8,
      handler: 'com.amazonaws.lambda.demo.LambdaFunctionHandler::handleRequest',
      description: `Generated on: ${new Date().toISOString()}`
                     
  });
  }
}