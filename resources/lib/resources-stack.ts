import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import {LambdaPipeline} from 'cdk-pipelines';


export interface ResourcesStackProps extends cdk.StackProps {
  readonly lambdaCode: lambda.CfnParametersCode;
}
export class ResourcesStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props: ResourcesStackProps) {
    super(scope, id, props);

    new LambdaPipeline(this, 'LambdaPipeline', {
      codeRepositoryName: 'javalambdasample',
      cdkFolderName: 'resources',
      lambdaCodeFolderName: '.',
      lambdaCode: props.lambdaCode,
      installCommands:  [ 
        'apt-get update -y',
        'apt-get install -y maven'
      ],
      buildCommands: [
        'mvn test',
        'mvn clean install',
        'ls',
        'cp ./target/demo-aws-lambda-package.zip .',
		'rm -r ./target/*',
		'cp demo-aws-lambda-package.zip ./target',
		'unzip ./target/demo-aws-lambda-package.zip -d ./target/',
		'rm ./target/demo-aws-lambda-package.zip',
		'ls ./target'
      ],
      artifactBaseDir: 'target',
      artifactFiles: ['**/*'],
      runtime: {java: 'openjdk8'}            
    }); 

  }
}
