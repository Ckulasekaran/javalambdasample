#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { ResourcesStack } from '../lib/resources-stack';
import { LambdaStack } from '../lib/lambda-stack';

const app = new cdk.App();
const lambdaStackk = new LambdaStack(app, 'LambdaStack');
new ResourcesStack(app, 'ResourcesStack'
, {
    lambdaCode: lambdaStackk.lambdaCode,    
    env: {
        region: 'us-east-2',
        account: '012834040237' 
    }        
});